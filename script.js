const gameContainer = document.getElementById("game");
const startBtn = document.getElementById('start');
const restartBtn = document.getElementById('restart');
const moveCount = document.getElementById('move-count');
const bestScoreCard = document.getElementById('best-score-number');
const messageContainer = document.getElementById('message-container');
const message = document.getElementById('message');
const gameInstruction = document.getElementById('game-instruction');
const move = document.getElementById('move');
gameContainer.style.display = 'none';
restartBtn.style.display = 'none';

const GIFS = [
  "./gifs/1.gif",
  "./gifs/2.gif",
  "./gifs/3.gif",
  "./gifs/4.gif",
  "./gifs/5.gif",
  "./gifs/6.gif",
];

function generateRandomCards() {
  const gamelevels = [4, 8, 12];
  const randomNumberIndex = Math.floor(Math.random() * gamelevels.length);
  return gamelevels[randomNumberIndex];
}
// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}
let randomNumber = generateRandomCards();
const shuffledGifs = shuffle(GIFS.slice(0, randomNumber / 2));
const cardPairs = [...shuffledGifs, ...shuffledGifs];
const shuffledCards = shuffle(cardPairs);


// this function loops over the array of Gifs
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForGifs(gifArray) {
  for (let gif of gifArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(gif);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}


// TODO: Implement this function!
function handleCardClick(event) {
  const clickedCard = event.target;
  clickedCard.classList.add('card-counter');
  const cardCounter = document.querySelectorAll('.card-counter');
  if (cardCounter.length <= 2) {
    const cardClasses = clickedCard.getAttribute('class').split(' ');
    const gifUrl = cardClasses[0];
    clickedCard.style.backgroundImage = `url(${gifUrl})`;
    clickedCard.classList.add('rotate-card');
    if (cardCounter.length === 2) {
      if (cardCounter[0].classList[0] === cardCounter[1].classList[0]) {
        for (let index = 0; index < cardCounter.length; index++) {
          cardCounter[index].removeEventListener('click', handleCardClick);
        }
        cardCounter[1].classList.add("card-matched");
        cardCounter[0].classList.add("card-matched");
        if (document.getElementsByClassName("card-matched").length === cardPairs.length) {
          gameContainer.style.display = 'none';
          if (localStorage.getItem("best-score") === null) {
            let bestScore = Number(moveCount.textContent) + 1;
            localStorage.setItem("best-score", `${bestScore}`);
            bestScoreCard.textContent = `${bestScore}`;
            messageContainer.style.display = 'flex';
            message.textContent = "Well Played!";
            restartBtn.style.display = 'flex';
          } else {
            let currBestScore = localStorage.getItem("best-score");
            if (Number(moveCount.textContent) + 1 <= Number(currBestScore)) {
              messageContainer.style.display = 'flex';
              message.textContent = "Congratulations,You won!";
              let newBestScore = Number(moveCount.textContent) + 1;
              localStorage.setItem("best-score", `${newBestScore}`);
              bestScoreCard.textContent = newBestScore;
              restartBtn.style.display = 'flex';
            } else {
              messageContainer.style.display = 'flex';
              message.textContent = "Great effort! You'll get it next time!";
              localStorage.setItem("best-score", `${currBestScore}`);
              bestScoreCard.textContent = currBestScore;
              restartBtn.style.display = 'flex';
            }
          }
        }
        closeMatchingCards(cardCounter);
      } else {
        setTimeout(() => {
          for (let index = 0; index < cardCounter.length; index++) {
            changeToOriginal(cardCounter[index]);
          }
          closeMismatchedCards(cardCounter);
        }, 1000);
      }
      if (moveCount.textContent === '0') {
        moveCount.textContent = 1;
      } else {
        moveCount.textContent = 1 + Number(moveCount.textContent);
      }
    }
  }
}

function changeToOriginal(card) {
  card.classList.remove('rotate-card');
  card.style.backgroundImage = `url("./images/dribble-04.jpg")`;
}

function closeMatchingCards(cards) {
  for (let index = 0; index < cards.length; index++) {
    cards[index].classList.remove('card-counter');
  }
}

function closeMismatchedCards(cards) {
  for (let index = 0; index < cards.length; index++) {
    cards[index].classList.remove('card-counter');
    changeToOriginal(cards[index]);
  }
}

// when the DOM loads
createDivsForGifs(shuffledCards);

startBtn.addEventListener('click', () => {
  gameContainer.style.display = 'flex';
  if (localStorage.getItem("best-score") === null) {
    bestScoreCard.textContent = 0;
  } else {
    bestScoreCard.textContent = localStorage.getItem("best-score");
  }
  gameInstruction.style.display = 'none';
  messageContainer.style.display = 'none';
  move.style.display = 'flex';
  startBtn.style.display = 'none';
});

restartBtn.addEventListener('click', () => {
  gameContainer.textContent = '';
  moveCount.textContent = 0;
  message.textContent = '';
  const newRandomNumber = generateRandomCards();
  const newShuffledGifs = shuffle(GIFS.slice(0, newRandomNumber / 2));
  const newCardPairs = [...newShuffledGifs, ...newShuffledGifs];
  const newShuffledCards = shuffle(newCardPairs);
  shuffledCards.length = 0;
  shuffledCards.push(...newShuffledCards);
  createDivsForGifs(shuffledCards);
  gameContainer.style.display = 'flex';
  messageContainer.style.display = 'none';
  move.style.display = 'flex';
  restartBtn.style.display = 'none';
});

window.addEventListener('load', () => {
  if (localStorage.getItem("best-score") === null) {
    bestScoreCard.textContent = 0;
  } else {
    bestScoreCard.textContent = localStorage.getItem("best-score");
  }
  messageContainer.style.display = 'none';
  move.style.display = 'none';
});